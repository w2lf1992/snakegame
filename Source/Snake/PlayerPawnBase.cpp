// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "UserWidgetBase.h"
#include "Components/InputComponent.h"

#include "Blueprint/WidgetTree.h"
//#include "Components/TextBlock.h"
//#include "Components/Button.h"
//#include "Components/Widget.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Initialize Camera component.
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;//Make camera the root.
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90,0,0));//Actor look down.
	CreateSnakeActor();

	//---------------------------------------------------------------------------------------------
	//Create widget
	UUserWidgetBase* HUD = NewObject<UUserWidgetBase>(this, WidgetClass, FName("HUD"));

	//Add this widget to viewport.
	HUD->AddToViewport();

	UWidgetTree* Tree = HUD->WidgetTree;//Get the subwidgets in HUD UserWidget.
	SnakeActor->ShowHUD = Tree->FindWidget(FName("ShowHUD"));//Set ref in snake for ShowHUD CanvasPanel
	SnakeActor->ScoreHUD = Tree->FindWidget(FName("TextBlock_Score"));//
	//Cast<UTextBlock>(ScoreHUD)->SetText();

	//Get playercontroller
	//auto PlayerController = GetFirstLocalPlayerController();
	SnakeActor->PlayerController = Cast<APlayerController>(GetController());
	//if (!ensure(PlayerController != nullptr)) return;

	////Setup an input mode. There are multiple such as game only or game and UI as well.
	//FInputModeUIOnly InputModeData;
	////Config is specific to the type
	//InputModeData.SetLockMouseToViewport(false);
	//InputModeData.SetWidgetToFocus(Menu->TakeWidget()); //Because UMG wraps Slate

	////Set the mode for the player controller
	//PlayerController->SetInputMode(InputModeData);

	//Enable cursor so you know what to click on:
	//PlayerController->bShowMouseCursor = true;
	//----------------------------------------------------------------------------------------------
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

//Spawn the snake actor class in the world.
void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

//Functions to change direction in which snake moves
//and also limit snake from going into it self.
void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection!=EMovementDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value2)
{
	if (IsValid(SnakeActor))
	{
		if (value2 > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
		else if (value2 < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}
	}
}

