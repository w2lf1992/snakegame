// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
//#include "Math/UnrealMathUtility.h"
//#include "UObject/ConstructorHelpers.h"
#include "Food.generated.h"

UENUM(BlueprintType)
enum class EFoodType : uint8
{
	GROW,
	SHRINK,
	FAST,
	SLOW
};

UCLASS()
class SNAKE_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	//My Vars
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;//FoodClass for later ref to respawn new food.

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* FoodMeshComponent;//The Food Mesh

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EFoodType FoodTypeEnum;//Type of food enum for switching between food.
	
	//UPROPERTY()
	FTimerHandle FoodTimerHandle;//To properly clear or pause your timer instance.
	AFood* NewFood;//For storing new food to respawn.
	bool bDoOnce = true;//To interact only once.
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	//My Functions
	void RespawnFood();
	void RespawnFoodRepeater();

};
