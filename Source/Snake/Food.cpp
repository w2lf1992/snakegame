// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "TimerManager.h"
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>
#include "SnakeBase.h"

#include "Kismet/KismetSystemLibrary.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create MeshComponent
	FoodMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));

	//Find Mesh and set it to MeshComponent.
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Engine/BasicShapes/Sphere"));//Find Mesh Asset.
	FoodMeshComponent->SetStaticMesh(SphereVisualAsset.Object);//Defualt Mesh
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

//If Snake food then...
void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead && bDoOnce)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			//Switch on type of food
			switch (FoodTypeEnum)
			{
			case EFoodType::GROW:
				Snake->AddSnakeElement();//add snake body
				break;
			case EFoodType::SHRINK:
				Snake->RemoveSnakeElement();//remove snake body
				break;
			case EFoodType::FAST:
				Snake->MovementSpeed += 1;//Speed Up
				break;
			case EFoodType::SLOW:
				Snake->MovementSpeed -= 1;//Speed Down
				break;
			}
			this->SetActorHiddenInGame(true);//Hide this Food
			bDoOnce = false;//DoOnce
			//Respawn Food with delay
			GetWorldTimerManager().SetTimer(
				FoodTimerHandle,
				this,
				&AFood::RespawnFood,
				1.f,
				false,
				(float)FMath::RandHelper(10));
		}
	}
}

void AFood::RespawnFood()
{
	//Spawn new food
	//---------------------------------------------------------------------------------------
	float SpawnLimit = FMath::RandRange(-400, 400);//Random number between the map size limit.
	FTransform SpawnTransform = FTransform(FVector(SpawnLimit, SpawnLimit, 20.f));//Make into FTransform

	//Respawn New Food if colliding repeat.
	while (!IsValid(NewFood))
	{
		NewFood = GetWorld()->SpawnActorDeferred<AFood>(
			FoodClass,
			FTransform::Identity,
			//SpawnTransform,
			nullptr,
			nullptr,
			ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding);
		UKismetSystemLibrary::PrintString(this, "Test: " + NewFood->GetName(), true, false, FLinearColor(0, 255, 0), 2);//Print
	}

	NewFood->FoodTypeEnum = this->FoodTypeEnum;//Set Food Type var

	//Finish Spawning (To allow initialize vars first. Like Mat in blueprint.)
	UGameplayStatics::FinishSpawningActor(NewFood, SpawnTransform);
	UKismetSystemLibrary::PrintString(this, "Test: " + NewFood->GetName(), true, false, FLinearColor(0, 255, 0), 2);//Print
	//----------------------------------------------------------------------------------------

	GetWorldTimerManager().ClearTimer(FoodTimerHandle);//Clear the timer.
	this->Destroy();//Destroy this food
}

void AFood::RespawnFoodRepeater()
{

}