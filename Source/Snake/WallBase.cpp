// Fill out your copyright notice in the Description page of Project Settings.


#include "WallBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
AWallBase::AWallBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create MeshComponent
	WallMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));

}

// Called when the game starts or when spawned
void AWallBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWallBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//If Snake food true then...
void AWallBase::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);//Get ASnakeBase from Interactor
		if (IsValid(Snake))
		{
			Snake->SnakeElements[0]->SetActorHiddenInGame(true);//Hide the snake head
			Snake->Destroy();//Destroy Snake
			Snake->ShowHUD->SetVisibility(ESlateVisibility::Visible);//ShowHUD
			Snake->PlayerController->bShowMouseCursor = true;//ShowMouse
		}
	}
}